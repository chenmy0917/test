import Vue from 'vue';
import InfoItem from './infoItem.vue';

// @ts-ignore
InfoItem.install = (vue: Vue) => {
  // @ts-ignore
    vue.component(InfoItem.componentName, InfoItem);
};

export default InfoItem;

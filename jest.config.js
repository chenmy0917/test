module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  globals: {
    'ts-jest': {
      diagnostics: {
        pathRegex: /\.(spec|test)\.ts$/
      }
    }
  },
  "collectCoverage": true,
  "collectCoverageFrom": ["components/**/*.vue", "!components/**/doc/*.{ts,vue}", "!components/**/components/**/*.{ts,vue}"]
};
